import matplotlib.pyplot as plt
from statistics import mean
import numpy as np
import pandas as pd

location_data = pd.read_csv("ds_compfest/catatan_lokasi.csv")
profile_data = pd.read_csv("ds_compfest/data_profil.csv")


lokasi_dominan = location_data.groupby("id").agg(lambda x: x.tolist())
profile = profile_data.groupby("id").agg(lambda x: x.tolist())

lokasi = lokasi_dominan["lokasi_dominan"]

all_lokasi = {"orang": [], "banyak_pindah": [],
              "umur": [], "divisi": [], "jenis_kelamin": []}
for i in range(len(lokasi)):
    all_lokasi["orang"].append(i+1)
    all_lokasi["banyak_pindah"].append(len(set(lokasi.iloc[[i]].tolist()[0])))
    all_lokasi["umur"].append(profile["umur"].iloc[[i]].tolist()[0][0]),
    all_lokasi["divisi"].append((profile["divisi"].iloc[[i]]).tolist()[0][0]),
    all_lokasi["jenis_kelamin"].append(
        profile["jenis_kelamin"].iloc[[i]].tolist()[0][0])

all_data = pd.DataFrame(all_lokasi)

print(all_data.describe(include="all"))
all_divisi = all_data.groupby("divisi").agg(
    lambda x: x.tolist())["banyak_pindah"]

average_pindah_divisi = {"divisi": ["Business Inteligence", "Customer Service",
                                    "Data Engineer", "Data Science", "Marketing", "Software Engineer", ], "rata_pindah": []}
for i in range(len(all_divisi)):
    average_pindah_divisi["rata_pindah"].append(
        mean(all_divisi.iloc[[i]].tolist()[0]))


rata2_pindah_divisi = pd.DataFrame(average_pindah_divisi)
rata2_pindah_divisi.plot(kind="bar", x="divisi", y="rata_pindah")


all_umur = all_data.groupby("umur").agg(lambda x: x.tolist())["banyak_pindah"]
umur = [i for i in range(21, 36)]
average_pindah_umur = {"umur": umur, "rata_pindah": []}
for i in range(len(all_umur)):
    average_pindah_umur["rata_pindah"].append(
        mean(all_umur.iloc[[i]].tolist()[0]))

rata2_pindah_umur = pd.DataFrame(average_pindah_umur)

rata2_pindah_umur.plot(kind="scatter", x="umur", y="rata_pindah")

kota = ["Jakarta Pusat", "Jakarta Barat", "Jakarta Selatan", "Jakarta Utara", "Jakarta Timur", "Kota Bogor", "Kabupaten Bogor",
        "Kota Depok", "Kota Tangerang", "Kota Tangerang Selatan", "Kabupaten Tangerang", "Kota Bekasi", "Kabupaten Bekasi"]

data = all_data.groupby("divisi").agg(lambda x: x.tolist())

data_mudik = {"id": [], "mudik": [], "divisi": []}

data_mudik_bisnis = {"id": data["orang"].tolist()[0], "mudik": [
]}


lst = lokasi.tolist()
for i in range(100):
    data_mudik["id"].append(i+1)
    data_mudik["divisi"].append(profile_data["divisi"].iloc[[i]].tolist()[0])
    temp = False
    for j in range(len(lst[i])):
        if lst[i][j] not in kota:
            temp = True
            break
    data_mudik["mudik"].append(temp)


df = pd.DataFrame(data_mudik)

print(df.groupby("divisi").agg(lambda x: x.tolist()))
